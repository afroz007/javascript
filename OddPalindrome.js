var num1 = 1500;
var num2 = 2000;
console.log(generateOddPalindromes(num1, num2));


function generateOddPalindromes( start, limit) {
    var str = "";
    if (start <= 0 || limit <= 0)
        return -1;
    if (start >= limit)
        return -2;
    for (var i = start; i <= limit; i++) {
        if (isPalindrome(i))
            if (isAllDigitsOdd(i) == true)

                str += i + ",";
    }

    return str.substring(0, str.length - 1);
}

function isPalindrome(num) {

    if (reverse(num) == num)
        return true;
    else
        return false;
}

function  reverse(num) {
    var rem = 0, rev = 0;
    while (num > 0) {
        rem = parseInt(num % 10);

        rev = parseInt(rev * 10 + rem);
        num =parseInt(num/ 10);
    }
    return rev;
}

function isAllDigitsOdd( i) {
    var count = 0, digit = 0;
    while (i > 0) {
        digit = parseInt(i % 10);
        if (digit % 2 == 0)
            return false;
        i =parseInt(i/ 10);

    }
    return true;

}

