function nextPalindrome(n) {

    function isPalindrome(num) {
        const forward = num.toString();
        let backward = [];
        for (let i = forward.length - 1; i >= 0; i--) {
            backward.push(forward[i]);
        }
        backward = backward.join('');
        return forward === backward;
    }

    let countUp = n + 1;

    let nextP;

    while (true) {
        if (isPalindrome(countUp)) {
            return countUp;
        } else {
            countUp += 1;
        }
    }
    console.log(countUp);
}
    console.log(nextPalindrome(123));


