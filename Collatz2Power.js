function collatzSequence(num)
{
    var max = 0;
    var sequence = [];
    sequence.push(num);
    while(num != 1)
    {
        if(num % 2 === 0)
            num = Math.floor(num / 2);
        else
            num = ( 3 * num + 1);
        sequence.push(num);
    }
    return sequence;
}



function highest_power_of_2_in_collatz_sequence(num)
{
    var highest = [];
    var sequence = collatzSequence(num);
    for(var i = 0; i <= sequence.length; i++)
        if(isPowerOfTwo(sequence[i]))
            highest.push(sequence[i]);

    var max =  Math.max.apply(null, highest);
    var i = 0;
    for(i = 1; i <= parseInt(max); i++)
    {

        if(Math.pow(2, i) === max)
            return i;

    }

}


function isPowerOfTwo(num)
{
    return Boolean (parseInt((Math.ceil((Math.log(num) / Math.log(2))))) ==
        parseInt((Math.floor(((Math.log(num) / Math.log(2)))))));
}

console.log(highest_power_of_2_in_collatz_sequence(257));